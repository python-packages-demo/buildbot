# [buildbot](https://pypi.org/project/buildbot/)

Automate the compile/test cycle https://buildbot.net/

* https://tracker.debian.org/pkg/buildbot
* https://github.com/buildbot/buildbot/
* https://libraries.io/pypi/buildbot
* https://en.wikipedia.org/wiki/Buildbot